#!/usr/bin/env zsh

# Copyright [2023] [Andy Wenk]

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

# http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

C_MARK="\033[32m\xE2\x9C\x94\033[0m"
E_MARK="\xE2\x9A\xA0\xEF\xB8\x8F "
MINICONDA_PATH="$HOME/miniconda"
OS=$(uname -m)

echo "This install script is intended to install Tensorflow on Mac OS X with arm64. It is inspired"
echo "by the Apple Developer instructions that can be found here: https://developer.apple.com/metal/tensorflow-plugin/"
echo "\n$E_MARK This script assumes you are using the ZSH Shell!"
echo "\nIt will install the following software:\n"
echo "  * miniconda"
echo "  * Tensorflow dependencies"
echo "  * Tensorflow for Mac OS X (2.9)"
echo "  * Apples Tensorflow Metal (0.5.0)\n"

printf "Do you want to continue (y|n)? " >&2
read -r option

if [ $option = "n" ]; then
  echo "Ok - stopping the installation!"
  exit;
fi

echo "\nChecking the type of the operating system."
if [ $OS = "arm64" ]; then
  echo "$C_MARK You are using arm64."
else
  echo "\n$E_MARK You are not using arm64. Aborting!"
  exit;
fi

echo "\nDownloading the conda environmnet into /tmp:\n"
cd /tmp
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-arm64.sh
chmod a+x /tmp/Miniconda3-latest-MacOSX-arm64.sh

echo "Running the Miniconda install script and creating the directory miniconda in $HOME."
/tmp/Miniconda3-latest-MacOSX-arm64.sh -b -p $MINICONDA_PATH

echo "\nInitializing conda, updateing your .zshrc file and sourcing .zshrc.\n"
$MINICONDA_PATH/bin/conda init zsh
source $HOME/.zshrc

echo "\nActivating the python env."
source $MINICONDA_PATH/bin/activate

echo "\nInstalling the software listed above."
conda install -c apple tensorflow-deps
python -m pip install tensorflow-macos==2.9
python -m pip install tensorflow-metal==0.5.0
python -m pip install numpy --upgrade

echo "\nCleanup the /tmp directory"
rm -rf /tmp/Miniconda3-latest-MacOSX-arm64.sh

echo "\nTesting if everything works correctly by firing a small python script. Please be patient."
python -c "import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))"

echo "\nIf the last output is something like 'tf.Tensor(-612.3172, shape=(), dtype=float32)' everything works."
echo "\nYou can now start working on Tensorflow projects. Remember to activate the environment and later deactivate it:\n"
echo "  * conda activate"
echo "  * conda deactivate"

echo "\n*** Bye ***"








