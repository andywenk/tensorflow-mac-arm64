# tensorflow-mac-arm64

## Abstract

This is a installscript for `zsh` and `Mac arm64` to install Tensorflow in a Python `venv`.

## Run it

To run the script, you need to make it executable first. Change to the directory where you put the script and run:

    ~ chmod a+x tensorflow-mac-arm64.sh

After that run:

    ~ ./tensorflow-mac-arm64.sh

## License

Copyright [2023] [Andy Wenk]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.